# Module magento 1.9.x pagebuilder, block builder, landing page

Magento 1.9.x page builder extension supports you to create and manage your website without knowing and writing a line of code. Just by carrying out some simple actions, you will have a website that meets the demand for beauty, creativity, harmony, and professionalism.

## Features
- Drag & Drop Interface FEATURED
- Magento WYSIWYG Integration
- 50+ Elements Supported
- Responsive Design Options
- Time-saving & Money-saving
- Work with any Magento Templates
- Convert Page & Elements into Content CMS Page/Block
- Create Unlimited Layouts
- Block Builder & Bootstrap Grid View System
- Easy To extend with 3rd Party extensions
- Cms Page Builder & CSS Skin Builder
- Product Page Builder
- Schedule Time To Display Your Page on Frontend
- Enhance page load time incredibly FEATURED
- Live Preview For Easier Editing

## PLEASE NOTE
- Magento 1 is end of life. You should upgrade the site to magento 2 soon as possible.


## Donation

If this project help you reduce time to develop, you can give me a cup of coffee :) 

[![paypal](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/paypalme/allorderdesk)


**Our Magento 2 Extensions List**
* [Megamenu for Magento 2](https://landofcoder.com/magento-2-mega-menu-pro.html/)

* [Page Builder for Magento 2](https://landofcoder.com/magento-2-page-builder.html/)

* [Magento 2 Marketplace - Multi Vendor Extension](https://landofcoder.com/magento-2-marketplace-extension.html/)

* [Magento 2 Multi Vendor Mobile App Builder](https://landofcoder.com/magento-2-multi-vendor-mobile-app.html/)

* [Magento 2 Form Builder](https://landofcoder.com/magento-2-form-builder.html/)

* [Magento 2 Reward Points](https://landofcoder.com/magento-2-reward-points.html/)

* [Magento 2 Flash Sales - Private Sales](https://landofcoder.com/magento-2-flash-sale.html)

* [Magento 2 B2B Packages](https://landofcoder.com/magento-2-b2b-extension-package.html)

* [Magento 2 One Step Checkout](https://landofcoder.com/magento-2-one-step-checkout.html/)

* [Magento 2 Customer Membership](https://landofcoder.com/magento-2-membership-extension.html/)

* [Magento 2 Checkout Success Page](https://landofcoder.com/magento-2-checkout-success-page.html/)


**Featured Magento Services**

* [Customization Service](https://landofcoder.com/magento-2-create-online-store/)

* [Magento 2 Support Ticket Service](https://landofcoder.com/magento-support-ticket.html/)

* [Magento 2 Multi Vendor Development](https://landofcoder.com/magento-2-create-marketplace/)

* [Magento Website Maintenance Service](https://landofcoder.com/magento-2-customization-service/)

* [Magento Professional Installation Service](https://landofcoder.com/magento-2-installation-service.html)

* [Customization Service](https://landofcoder.com/magento-customization-service.html)
